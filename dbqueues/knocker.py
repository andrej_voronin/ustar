import os
from cStringIO import StringIO
import types

try:
    import json
except ImportError:
    import simplejson as json

import sqlite3 as sqlite
import uuid
import time

from twisted.python import log
from twisted.spread import pb
from twisted.web import xmlrpc, resource, server
from twisted.internet import defer
from twisted.application import service

class KnockerDBqueueQueueResource(resource.Resource):
    isLeaf = True
    def __init__(self, client, storage):
        self.client = client
        self.storage = storage
    def deleteSuccess(self, result, queue_id, request, d):
        title = 'Knocker db queue forms interface'
        path = '/'.join(request.prepath)
        self.client.saveQueues(self.storage)
        sio = StringIO()
        request.setHeader("Content-Type", "text/html; charset=utf-8")
        h1 = 'Deleting queue %s' % (queue_id,)
        refresh_path = "/%s/list" % (path,)
        sio.write('<html><head><meta http-equiv="Refresh" content="1;url=%s"/><title>%s</title></head><body>' % (refresh_path, title))
        sio.write('<h1>%s</h1>' % (h1,))
        sio.write('<p>Queue deleted</p>')
        sio.write('</body></html>')
        s = sio.getvalue()
        sio.close()
        if type(s) == types.UnicodeType:
            s = s.encode('utf-8')
        request.write(s)
        request.finish()
    def deleteFailure(self, failure, queue_id, request, d):
        title = 'Knocker db queue forms interface'
        path = '/'.join(request.prepath)
        sio = StringIO()
        request.setHeader("Content-Type", "text/html; charset=utf-8")
        h1 = 'Deleting queue %s' % (queue_id,)
        refresh_path = "/%s/list" % (path,)
        sio.write('<html><head><meta http-equiv="Refresh" content="1;url=%s"/><title>%s</title></head><body>' % (refresh_path, title))
        sio.write('<h1>%s</h1>' % (h1,))
        sio.write('<p>Error deleting queue</p>')
        sio.write('</body></html>')
        s = sio.getvalue()
        sio.close()
        if type(s) == types.UnicodeType:
            s = s.encode('utf-8')
        request.write(s)
        request.finish()
    def render_GET(self, request):
        path = '/'.join(request.prepath)
        title = 'Knocker db queue forms interface'
        if len(request.postpath) > 0:
            method = request.postpath[0]
            if method == '':
                method = 'list'
        else:
            method = 'list'
        if method == 'delete':
            queue_id = request.postpath[1]
            d = defer.Deferred()
            self.client.deleteQueue(queue_id).addCallback(lambda x: self.deleteSuccess(x, queue_id, request, d)).addErrback(lambda x:self.deleteFailure(x, queue_id, request, d))
            return server.NOT_DONE_YET
        elif method == 'edit':
            sio = StringIO()
            request.setHeader("Content-Type", "text/html; charset=utf-8")
            queue_id = request.postpath[1]
            rule = request.postpath[2]
            h1 = 'Editing queue %s' % (queue_id,)
            refresh_path = "/%s/view/%s" % (path, queue_id)
            sio.write('<html><meta http-equiv="Refresh" content="1;url=%s"/><title>%s</title><body>' % (refresh_path, title))
            sio.write('<h1>%s</h1>' % (h1,))
            self.client.removeQueueRule(queue_id, rule)
            self.client.saveQueues(self.storage)
            sio.write('Rule deleted')
            sio.write('</body></html>')
            s = sio.getvalue()
            sio.close()
            if type(s) == types.UnicodeType:
                s = s.encode('utf-8')
            return s
        elif method == 'view':
            sio = StringIO()
            request.setHeader("Content-Type", "text/html; charset=utf-8")
            queue_id = request.postpath[1]
            h1 = 'Viewing queue %s' % (queue_id,)
            form_title = "Add rule"
            sio.write('<html><title>%s</title><body>' % (title,))
            home_path = "/%s/list" % (path,)
            sio.write('<a href="%s">Home</a>' % (home_path,))
            sio.write('<h1>%s</h1>' % (h1,))
            rules = self.client.getQueueRules(queue_id)
            if len(rules) > 0:
                sio.write('<ul>')
                for r in rules:
                    sio.write('<li>%s <a href="/%s/edit/%s/%s">Delete</a></li>' % (r['Rule'], path, queue_id, r['Rule']))
                sio.write('</ul>')
            else:
                sio.write('<p>No rules added.</p>')
            sio.write('<form method="post" action="/%s/edit/%s">' % (path, queue_id))
            sio.write('<h2>%s</h2>' % (form_title,))
            sio.write('<table border="0">')
            sio.write('<tr><td>Rule</td><td><input name="rule" type="text" size="10"/></td></tr>')
            sio.write('<tr><td colspan="2" align="right"><input type="submit" value="Add"/></td></tr>')
            sio.write('<form>')
            sio.write('</body></html>')
            s = sio.getvalue()
            sio.close()
            if type(s) == types.UnicodeType:
                s = s.encode('utf-8')
            return s
        elif method == 'list':
            sio = StringIO()
            request.setHeader("Content-Type", "text/html; charset=utf-8")
            title = 'Knocker db queue forms interface'
            h1 = 'Queues list'
            form_title = 'Add new queue'
            sio.write('<html><title>%s</title><body>' % (title,))
            sio.write('<h1>%s</h1>' % (h1,))
            queues = self.client.listQueues()
            if len(queues) > 0:
                sio.write('<ul>')
                for q in queues:
                    queue_id = q['QueueId']
                    queue_comment = q['QueueComment']
                    stat = self.client.getQueueStat(queue_id)
                    sended = stat['Sended']
                    sio.write('<li><a href="/%s/view/%s">Queue %s(%s)</a> %d <a href="/%s/delete/%s">Delete</a></li>' % (path, queue_id, queue_id, queue_comment, sended, path, queue_id))
                sio.write('</ul>')
            else:
                sio.write('<p>No queues defined.</p>')
            sio.write('<form method="post" action="/%s/add">' % (path, ))
            sio.write('<h2>%s</h2>' % (form_title,))
            sio.write('<table border="0">')
            sio.write('<tr><td>Comment</td><td><input name="comment" type="text" size="40"/></td></tr>')
            sio.write('<tr><td colspan="2" align="right"><input type="submit" value="Add"/></td></tr>')
            sio.write('<form>')
            sio.write('</table>')
            sio.write('</body></html>')
            s = sio.getvalue()
            sio.close()
            if type(s) == types.UnicodeType:
                s = s.encode('utf-8')
            return s
        else:
            sio = StringIO()
            request.setHeader("Content-Type", "text/html; charset=utf-8")
            sio.write('<html><title>%s</title><body>' % (title,))
            sio.write('Unknown method %s' % (method,))
            sio.write('</body></html>')
            s = sio.getvalue()
            sio.close()
            if type(s) == types.UnicodeType:
                s = s.encode('utf-8')
            return s
    def addSuccess(self, queue_id, request, d):
        title = 'Knocker db queue forms interface'
        path = '/'.join(request.prepath)
        self.client.saveQueues(self.storage)
        sio = StringIO()
        request.setHeader("Content-Type", "text/html; charset=utf-8")
        h1 = 'Adding new queue'
        refresh_path = "/%s/list" % (path,)
        sio.write('<html><head><meta http-equiv="Refresh" content="1;url=%s"/><title>%s</title></head><body>' % (refresh_path, title))
        sio.write('<h1>%s</h1>' % (h1,))
        sio.write('<p>Queue added</p>')
        sio.write('</body></html>')
        s = sio.getvalue()
        sio.close()
        if type(s) == types.UnicodeType:
            s = s.encode('utf-8')
        request.write(s)
        request.finish()
    def addFailure(self, failure, request, d):
        title = 'Knocker client forms interface'
        path = '/'.join(request.prepath)
        sio = StringIO()
        request.setHeader("Content-Type", "text/html; charset=utf-8")
        h1 = 'Adding new queue'
        refresh_path = "/%s/list" % (path,)
        sio.write('<html><head><meta http-equiv="Refresh" content="1;url=%s"/><title>%s</title></head><body>' % (refresh_path, title))
        sio.write('<h1>%s</h1>' % (h1,))
        sio.write('<p>Error adding queue</p>')
        sio.write('</body></html>')
        s = sio.getvalue()
        sio.close()
        if type(s) == types.UnicodeType:
            s = s.encode('utf-8')
        request.write(s)
        request.finish()
    def render_POST(self, request):
        path = '/'.join(request.prepath)
        method = request.postpath[0]
        title = 'Knocker client forms interface'
        if method == 'add':
            comment = request.args['comment'][0]
            d = defer.Deferred()
            self.client.createQueue(comment).addCallback(lambda x:self.addSuccess(x, request, d)).addErrback(lambda x:self.addFailure(x, request, d))
            return server.NOT_DONE_YET
        elif method == 'edit':
            sio = StringIO()
            request.setHeader("Content-Type", "text/html; charset=utf-8")
            queue_id = request.postpath[1]
            rule = request.args['rule'][0]
            h1 = 'Editing queue %s' % (queue_id,)
            refresh_path = "/%s/view/%s" % (path, queue_id)
            sio.write('<html><head><meta http-equiv="Refresh" content="1;url=%s"/><title>%s</title></head><body>' % (refresh_path, title))
            sio.write('<h1>%s</h1>' % (h1,))
            self.client.appendQueueRule(queue_id, rule)
            self.client.saveQueues(self.storage)
            sio.write('Rule added')
            sio.write('</body></html>')
            s = sio.getvalue()
            sio.close()
            if type(s) == types.UnicodeType:
                s = s.encode('utf-8')
            return s
        else:
            sio = StringIO()
            request.setHeader("Content-Type", "text/html; charset=utf-8")
            sio.write('<html><title>%s</title><body>' % (title,))
            sio.write('Unknown method %s' % (method,))
            sio.write('</body></html>')
            s = sio.getvalue()
            sio.close()
            if type(s) == types.UnicodeType:
                s = s.encode('utf-8')
            return s

class KnockerDBqueueRootResource(resource.Resource):
    isLeaf = False
    def __init__(self, client, storage):
        self.client = client
        self.storage = storage
        resource.Resource.__init__(self)
        self.putChild("queue", KnockerDBqueueQueueResource(self.client, self.storage))
    def render(self, request):
        path = '/'.join(request.prepath)
        request.redirect("/%s/queue/list" % (path,))
        request.finish()
        return server.NOT_DONE_YET

class KnockerDBqueuePbResource(pb.Root):
    def __init__(self, client, storage):
        self.client = client
        self.storage = storage
    def createQueueSuccess(self, result, d):
        self.client.saveQueues(self.storage)
        d.callback(result)
    def createQueueFailure(self, failure, d):
        d.errback(failure)
    def remote_createQueue(self, comment):
        d = defer.Deferred()
        self.client.createQueue(comment).addCallback(lambda x:self.createQueueSuccess(x, d)).addErrback(lambda x:self.createQueueFailure(x, d))
        return d
    def deleteQueueSuccess(self, result, d):
        self.client.saveQueues(self.storage)
        d.callback(result)
    def deleteQueueFailure(self, failure, d):
        d.errback(failure)
    def remote_deleteQueue(self, q_id):
        d = defer.Deferred()
        self.client.deleteQueue(q_id).addCallback(lambda x:self.deleteQueueSuccess(x, d)).addErrback(lambda x:self.deleteQueueFailure(x, d))
        return d
    def remote_listQueues(self):
        return self.client.listQueues()
    def remote_appendQueueRule(self, q_id, rule):
        err = self.client.appendQueueRule(self, q_id, rule)
        self.client.saveQueues(self.storage)
        return err
    def remote_removeQueueRule(self, q_id, rule):
        err = self.client.removeQueueRule(self, q_id, rule)
        self.client.saveQueues(self.storage)
        return err
    def remote_getQueueRules(self, q_id):
        return self.client.getQueueRules(q_id)
    def remote_getQueueStat(self, q_id):
        return self.client.getQueueStat(q_id)

class KnockerDBqueueXmlrpcResource(xmlrpc.XMLRPC):
    def __init__(self, client, storage):
        xmlrpc.XMLRPC.__init__(self)
        self.client = client
        self.storage = storage
    def createQueueSuccess(self, result, d):
        self.client.saveQueues(self.storage)
        d.callback(result)
    def createQueueFailure(self, failure, d):
        d.errback(failure)
    def xmlrpc_createQueue(self, comment):
        d = defer.Deferred()
        self.client.createQueue(comment).addCallback(lambda x:self.createQueueSuccess(x, d)).addErrback(lambda x:self.createQueueFailure(x, d))
        return d
    def deleteQueueSuccess(self, result, d):
        self.client.saveQueues(self.storage)
        d.callback(result)
    def deleteQueueFailure(self, failure, d):
        d.errback(failure)
    def xmlrpc_deleteQueue(self, q_id):
        d = defer.Deferred()
        self.client.deleteQueue(q_id).addCallback(lambda x:self.deleteQueueSuccess(x, d)).addErrback(lambda x:self.deleteQueueFailure(x, d))
        return d
    def xmlrpc_listQueues(self):
        return self.client.listQueues()
    def xmlrpc_appendQueueRule(self, q_id, rule):
        err = self.client.appendQueueRule(self, q_id, rule)
        self.client.saveQueues(self.storage)
        return err
    def xmlrpc_removeQueueRule(self, q_id, rule):
        err = self.client.removeQueueRule(self, q_id, rule)
        self.client.saveQueues(self.storage)
        return err
    def xmlrpc_getQueueRules(self, q_id):
        return self.client.getQueueRules(q_id)
    def xmlrpc_getQueueStat(self, q_id):
        return self.client.getQueueStat(q_id)

class KnockerDBqueueSqliteStorage:
    def __init__(self, dbname):
        self.dbname = dbname
        if not os.access(self.dbname, os.F_OK):
            self.dbcon = sqlite.connect(self.dbname)
            self.initDatabase()
        else:
            self.dbcon = sqlite.connect(self.dbname)
    def initDatabase(self):
        c = self.dbcon.cursor()
        sql = '''CREATE TABLE QUEUES(id integer not null,
                                created_time integer not null,
                                queue_id text not null,
                                name text not null,
                                comment text not null,
                                PRIMARY KEY(id))
        '''
        c.execute(sql)
        sql = '''CREATE TABLE QUEUERULES(id integer not null,
                                queue_table_id integer not null,
                                rule text not null,
                                created_time integer not null,
                                PRIMARY KEY(id))
        '''
        c.execute(sql)
        sql = '''CREATE INDEX QUEUERULES_BY_QUEUETABLEID ON QUEUERULES(queue_table_id)'''
        c.execute(sql)
        self.dbcon.commit()
        c.close()
    def deleteAll(self):
        c = self.dbcon.cursor()
        sql = '''DELETE FROM QUEUES
        '''
        c.execute(sql)
        sql = '''DELETE FROM QUEUERULES
        '''
        c.execute(sql)
        self.dbcon.commit()
        c.close()
    def save(self, queues):
        self.deleteAll()
        c = self.dbcon.cursor()
        for queue_id, queue_name, queue_comment, rules, sended in queues.itervalues():
            ts_now = int(time.time())
            try:
                c.execute('INSERT INTO QUEUES(created_time, queue_id, name, comment) VALUES(?, ?, ?, ?)', (ts_now, queue_id, queue_name, queue_comment))
            except sqlite.Error, why:
                log.msg('KnockerDBqueueSqliteStorage:save:db error:%s' % (why))
                c.close()
                return
            queue_table_id = c.lastrowid
            for rule in rules.iterkeys():
                try:
                    c.execute('INSERT INTO QUEUERULES(queue_table_id, created_time, rule) VALUES(?, ?, ?)', (queue_table_id, ts_now, rule))
                except sqlite.Error, why:
                    log.msg('KnockerDBqueueSqliteStorage:save:db error:%s' % (why))
                    c.close()
                    return
        self.dbcon.commit()
        c.close()
    def load(self):
        ress = []
        c = self.dbcon.cursor()
        try:
            c.execute('SELECT id, created_time, queue_id, name, comment FROM QUEUES')
        except sqlite.Error, why:
            log.msg('KnockerDBqueueSqliteStorage:load:db error:%s' % (why))
            c.close()
            return []
        r = c.fetchone()
        while None != r:
            ress.append(dict(
                queue_table_id=r[0],
                created_time=r[1],
                queue_id=r[2],
                queue_name=r[3],
                queue_comment=r[4],
                rules=[]
            ))
            r = c.fetchone()
        for res in ress:
            try:
                c.execute('SELECT rule, created_time FROM QUEUERULES WHERE queue_table_id=? ORDER BY id', (res['queue_table_id'],))
            except sqlite.Error, why:
                log.msg('KnockerDBqueueSqliteStorage:load:db error:%s' % (why))
                c.close()
                return []
            r = c.fetchone()
            while None != r:
                res['rules'].append(dict(
                    rule=r[0],
                    created_time=r[1],
                ))
                r = c.fetchone()
        c.close()
        return ress

f_s = {}
def convert_to_knocker_struct(s):
    global f_s
    p = []
    if s.has_key('Index'):
        flash_addr = s['Index']
    else:
        flash_addr = None
    p.append({'Type':'Index', 'StateTime':s['StateTime'], 'FlashAddr':flash_addr, 'EquipId':s['EquipId']})
    if s.has_key('Location'):
        gps_fix = True
        if s.has_key('Flags'):
            if s['Flags'].has_key('GpsFix'):
                gps_fix = s['Flags']['GpsFix']
        t = {'Type':'Location', 'Data':[]}
        t['Data'].append({'Type':'Value', 'Latitude':s['Location']['Latitude'], 'Longitude':s['Location']['Longitude'], 'Speed':s['Location']['Velocity'], 'Altitude':s['Location']['Height'], 'Course':s['Location']['Course']})
        t['Data'].append({'Type':'Reliability', 'FixTime':s['Location']['LocationTime'], 'Satellites':None, 'Fix':gps_fix})
        p.append(t)
    if s.has_key('State'):
        t = {'Type':'Inputs','Data':[]}
        if s['State'].has_key('Adcs'):
            if s['State']['Adcs'].has_key('1') and s['State']['Adcs'].has_key('2') and s['State']['Adcs'].has_key('3') and s['State']['Adcs'].has_key('4'):
                t['Data'].append({'Type':'Voltmeter', 'Values':[s['State']['Adcs']['1'],s['State']['Adcs']['2'], s['State']['Adcs']['3'], s['State']['Adcs']['4']]})
        if s['State'].has_key('Sensors'):
            if s['State']['Sensors'].has_key('1') and s['State']['Sensors'].has_key('2') and s['State']['Sensors'].has_key('3') and s['State']['Sensors'].has_key('4') and s['State']['Sensors'].has_key('5') and s['State']['Sensors'].has_key('6') and s['State']['Sensors'].has_key('7') and s['State']['Sensors'].has_key('8'):
                t['Data'].append({'Type':'Logic', 'Values':[s['State']['Sensors']['1'], s['State']['Sensors']['2'], s['State']['Sensors']['3'], s['State']['Sensors']['4'], s['State']['Sensors']['5'], s['State']['Sensors']['6'], s['State']['Sensors']['7'], s['State']['Sensors']['8']]})
        if s.has_key('FuelSensorId'):
            f_s[s['EquipId']] = s
        else:
            if s.has_key('FuelSensor'):
                tf = {'Type':'FuelSensor', 'Values':[]}
                for k in ('1','2','3','7','8'):
                    if s['FuelSensor']['Level'].has_key(k):
                        if s['FuelSensor']['Level'][k] >= 0:
                            tf['Values'].append({'SensorId':(s['EquipId'] & 0xffff) * 10 + int(k), 'MeasurementTime':s['StateTime'], 'SensorOutput':int(s['FuelSensor']['Level'][k])})
                t['Data'].append(tf)
            else:
                if f_s.has_key(s['EquipId']):
                    if s.has_key('Flags'):
                        if s['Flags'].has_key('Online'):
                             if s['Flags']['Online']:
                                    if None != s['StateTime'] and None != f_s[s['EquipId']]['StateTime']:
                                        if s['StateTime'] >= f_s[s['EquipId']]['StateTime']:
                                            tf = {'Type':'FuelSensor', 'Values':[]}
                                            for k in ('1','2'):
                                                if f_s[s['EquipId']]['FuelSensor']['Level'].has_key(k):
                                                    if f_s[s['EquipId']]['FuelSensor']['Level'][k] >= 0:
                                                        tf['Values'].append({'SensorId':(f_s[s['EquipId']]['FuelSensorId'] & 0xffff) * 10 + int(k), 'MeasurementTime':f_s[s['EquipId']]['StateTime'], 'SensorOutput':int(f_s[s['EquipId']]['FuelSensor']['Level'][k])})
                                            t['Data'].append(tf)

        p.append(t)
        if s['State'].has_key('BatteryVoltage'):
            battery = s['State']['BatteryVoltage']
        else:
            battery = None
        if s['State'].has_key('SupplyVoltage'):
            power = s['State']['SupplyVoltage']
        else:
            power = None
        p.append({'Type':'EquipSupply', 'Power':power, 'Battery':battery, 'Current':None})

        if s['State'].has_key('Outs'):
            t = {'Type':'Outs', 'Data':[]}
            t['Data'].append({'Type':'Key', 'Values':[s['State']['Outs']['1'], s['State']['Outs']['2'], s['State']['Outs']['3'], s['State']['Outs']['4']]})
            p.append(t)
        if s.has_key('AlarmZones') and  s.has_key('AlarmEvent') and s.has_key('AlarmTime'):
            p.append({'Type':'AlarmZones',
                             'Now':[s['AlarmZones']['AlarmsNow']['1'], s['AlarmZones']['AlarmsNow']['2'], s['AlarmZones']['AlarmsNow']['3'], s['AlarmZones']['AlarmsNow']['4'], s['AlarmZones']['AlarmsNow']['5'], s['AlarmZones']['AlarmsNow']['6'], s['AlarmZones']['AlarmsNow']['7'], s['AlarmZones']['AlarmsNow']['8']],
                             'Alarms':[s['AlarmZones']['Alarms']['1'], s['AlarmZones']['Alarms']['2'], s['AlarmZones']['Alarms']['3'], s['AlarmZones']['Alarms']['4'], s['AlarmZones']['Alarms']['5'], s['AlarmZones']['Alarms']['6'], s['AlarmZones']['Alarms']['7'], s['AlarmZones']['Alarms']['8']],
                             'Old':[s['AlarmZones']['AlarmsOld']['1'], s['AlarmZones']['AlarmsOld']['2'], s['AlarmZones']['AlarmsOld']['3'], s['AlarmZones']['AlarmsOld']['4'], s['AlarmZones']['AlarmsOld']['5'], s['AlarmZones']['AlarmsOld']['6'], s['AlarmZones']['AlarmsOld']['7'], s['AlarmZones']['AlarmsOld']['8']]
                        })
        if s.has_key('FMSData'):
            t = {'Type':'CanBus','Data':[]}
            t['Data'].append({'Type':'FMS','TotalFuel':s['FMSData']['FuelUsed'], 'TotalDistance':s['FMSData']['VehicleDistance'], 'FuelLevel':s['FMSData']['FuelLevel'], 'CoolantTemperature':s['FMSData']['EngineTemperature'],'RPM':s['FMSData']['EngineSpeed']})
            p.append(t)

    return {"Type":'Report', "SenderId":s['EquipId'], "ReceivedTime":s['CreatedTime'], 'InPacketIndex':1, "Payload":p}

class KnockerBaseDBqueueService(service.Service):
    def __init__(self, dbpool):
        self.dbpool = dbpool
    create_queue_table_tmpl = '''CREATE TABLE %s (
    ID bigint(11) unsigned NOT NULL AUTO_INCREMENT,
    TIME_STAMP timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    DIRECTION varchar(3) NOT NULL,
    PACKET text NOT NULL,
    ERROR tinyint(1) DEFAULT NULL,
    PRIMARY KEY (ID))
    '''
    delete_queue_table_tmpl = '''DROP TABLE %s'''

    def insertRecord(self, queue_name, j_p, callback, errback):
        if self.dbpool.dbapi.paramstyle == 'format':
            s = "INSERT INTO %s(DIRECTION,PACKET) VALUES(%%s,%%s)" % (queue_name,)
            self.dbpool.runOperation(s, ("IN", j_p)).addCallback(callback).addErrback(errback)
        elif self.dbpool.dbapi.paramstyle == 'qmark':
            s = "INSERT INTO %s(DIRECTION,PACKET) VALUES(?,?)" % (queue_name,)
            self.dbpool.runOperation(s, ("IN", j_p)).addCallback(callback).addErrback(errback)
        elif self.dbpool.dbapi.paramstyle == 'pyformat':
            s = "INSERT INTO %s(DIRECTION,PACKET) VALUES(%%(dir)s,%%(message)s)" % (queue_name,)
            self.dbpool.runOperation(s, {'dir':"IN", 'message':j_p}).addCallback(callback).addErrback(errback)
        elif self.dbpool.dbapi.paramstyle == 'numeric':
            s = "INSERT INTO %s(DIRECTION,PACKET) VALUES(:1,:2)" % (queue_name,)
            self.dbpool.runOperation(s, ("IN", j_p)).addCallback(callback).addErrback(errback)
        elif self.dbpool.dbapi.paramstyle == 'named':
            s = "INSERT INTO %s(DIRECTION,PACKET) VALUES(:dir,:message)" % (queue_name,)
            self.dbpool.runOperation(s, {'dir':"IN", 'message':j_p}).addCallback(callback).addErrback(errback)
        else:
            log.msg("KnockerBaseDBqueueService:unknown paramstyle %s for dbapi %s" % (self.dbpool.dbapi.paramstyle,self.dbpool.dbapiName))

    def createTable(self, table_name, callback, errback):
        self.dbpool.runOperation(self.create_queue_table_tmpl % (table_name,)).addCallback(callback).addErrback(errback)

    def deleteTable(self, table_name, callback, errback):
        self.dbpool.runOperation(self.delete_queue_table_tmpl % (table_name,)).addCallback(callback).addErrback(errback)

class KnockerOneDBqueueService(KnockerBaseDBqueueService):
    def __init__(self, dbpool, queue_name):
        KnockerBaseDBqueueService.__init__(self, dbpool)
        self.queue_name = queue_name

    def insertSuccess(self, result):
        #log.msg("KnockerOneDBqueueService:record inserted, result : %r" % (result,))
        pass

    def insertFailure(self, failure):
        log.msg("KnockerOneDBqueueService:record failed to insert : %s" % (failure.getTraceback()))

    def putMessage(self, msg):
        j_p = json.dumps(convert_to_knocker_struct(msg))
        self.insertRecord(self.queue_name, j_p, self.insertSuccess, self.insertFailure)

    def createTable(self, callback, errback):
        KnockerBaseDBqueueService.createTable(self, self.queue_name, callback, errback)

    def deleteTable(self, callback, errback):
        KnockerBaseDBqueueService.deleteTable(self, self.queue_name, callback, errback)

class KnockerMultiplyDBqueueService(KnockerBaseDBqueueService):
    def __init__(self, dbpool):
        KnockerBaseDBqueueService.__init__(self, dbpool)
        self.queues = {}

    def loadQueues(self, storage):
        q_ss = storage.load()
        for q_s in q_ss:
            self.queues[q_s['queue_id']] = [q_s['queue_id'], q_s['queue_name'], q_s['queue_comment'], {}, 0]
            for r_s in q_s['rules']:
                self.queues[q_s['queue_id']][3][r_s['rule']] = r_s['rule']

    def saveQueues(self, storage):
        storage.save(self.queues)

    def createQueueSuccess(self, result, queue_id, queue_name, queue_comment, d):
        self.queues[queue_id] = [queue_id, queue_name, queue_comment, {}, 0]
        d.callback(queue_id)

    def createQueueFailure(self, failure, queue_id, queue_name, queue_comment, d):
        log.msg("KnockerMultiplyDBqueueService:create queue %s failure: %s" % (queue_id, failure.getTraceback()))
        d.errback(failure)

    def createQueue(self, queue_comment = ''):
        queue_id = str(uuid.uuid4()).replace('-','_')
        queue_name = 'Q_%s' % (queue_id,)
        d = defer.Deferred()
        self.createTable(queue_name, lambda x:self.createQueueSuccess(x, queue_id, queue_name, queue_comment, d), lambda x:self.createQueueFailure(x, queue_id, queue_name, queue_comment, d))
        return d

    def deleteQueueSuccess(self, result, queue_id, d):
        d.callback(True)

    def deleteQueueFailure(self, failure, queue_id, d):
        log.msg("KnockerMultiplyDBqueueService:delete queue %s failure: %s" % (queue_id, failure.getTraceback()))
        d.errback(failure)

    def deleteQueue(self, q_id):
        if self.queues.has_key(q_id):
            del self.queues[q_id]
            queue_name = 'Q_%s' % (q_id,)
            d = defer.Deferred()
            self.deleteTable(queue_name,lambda x:self.deleteQueueSuccess(x, q_id, d),lambda x:self.deleteQueueFailure(x, q_id, d))
            return d
        else:
            return False

    def getQueueStat(self, q_id):
        if self.queues.has_key(q_id):
            return {'Sended':self.queues[q_id][4]}
        else:
            return None

    def listQueues(self):
        q_ids = []
        for v in self.queues.itervalues():
            q_ids.append({'QueueId':v[0], 'QueueName':v[1], 'QueueComment':v[2]})
        return q_ids

    def appendQueueRule(self, q_id, rule):
        if self.queues.has_key(q_id):
            self.queues[q_id][3][rule] = rule
            return True
        else:
            return False

    def removeQueueRule(self, q_id, rule):
        if self.queues.has_key(q_id):
            if self.queues[q_id][3].has_key(rule):
                del self.queues[q_id][3][rule]
                return True
            else:
                return False
        else:
            return False

    def getQueueRules(self, q_id):
        rules = []
        if self.queues.has_key(q_id):
            for k in self.queues[q_id][3].iterkeys():
                rules.append({'Rule':k})
            return rules
        else:
            return []

    def insertSuccess(self, result, queue_id):
        #log.msg("KnockerMultiplyDBqueueService:queue %s record inserted, result : %r" % (queue_id, result))
        self.queues[queue_id][4] = self.queues[queue_id][4] + 1

    def insertFailure(self, failure, queue_id):
        queue_name = self.queues[queue_id][1]
        log.msg("KnockerMultiplyDBqueueService:record failed to insert to %s: %s" % (queue_name, failure.getTraceback()))

    def putMessage(self, msg):
        serial = msg['EquipId']
        for queue_id in self.queues.iterkeys():
            if self.queues[queue_id][3].has_key('*') or self.queues[queue_id][3].has_key('%d' % serial):
                queue_name = self.queues[queue_id][1]
                j_p = json.dumps(convert_to_knocker_struct(msg))
                self.insertRecord(self.queue_name, j_p, lambda x,arg=queue_id:self.insertSuccess(x, arg), lambda x,arg=queue_id:self.insertFailure(x, arg))
