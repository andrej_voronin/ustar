from twisted.python import log
from twisted.application.service import Service
from twisted.internet import defer, reactor

class DatabaseQueueService(Service):
    max_unprocessed = 100
    def __init__(self, dbpool_export, q_name):
        self.dbpool_export = dbpool_export
        self.fetched = 0
        self.stored = 0
        self.deleted = 0
        self.paused = False
        self.paused_mid = None
        self.unprocessed = 0
        self.q_name = q_name
    def printStat(self):
        #log.msg('DatabaseQueueService: fetched %d, stored %d, deleted %s' % (self.fetched, self.stored, self.deleted))
        pass
    def deleteMessageFailure(self, failure, mid):
        log.msg('DatabaseQueueService: error deleting message %d: %s' % (mid, failure.getTraceback()))
        reactor.callLater(3, self.deleteMessage, mid)
    def deleteMessageSuccess(self, result, mid):
        self.deleted = self.deleted + 1
        self.printStat()
    def deleteMessage(self, mid):
        if self.dbpool_export.dbapi.paramstyle == 'format':
            s = 'DELETE FROM %s WHERE ID=%%s' % (self.q_name,)
            self.dbpool_export.runOperation(s, (mid,)).addCallback(lambda x:self.deleteMessageSuccess(x, mid)).addErrback(lambda x:self.deleteMessageFailure(x, mid))
        elif self.dbpool_export.dbapi.paramstyle == 'qmark':
            s = 'DELETE FROM %s WHERE ID=?' % (self.q_name,)
            self.dbpool_export.runOperation(s, (mid,)).addCallback(lambda x:self.deleteMessageSuccess(x, mid)).addErrback(lambda x:self.deleteMessageFailure(x, mid))
        elif self.dbpool_export.dbapi.paramstyle == 'pyformat':
            s = 'DELETE FROM %s WHERE ID=%%(mid)s' % (self.q_name,)
            self.dbpool_export.runOperation(s, {'mid':mid}).addCallback(lambda x:self.deleteMessageSuccess(x, mid)).addErrback(lambda x:self.deleteMessageFailure(x, mid))
        elif self.dbpool_export.dbapi.paramstyle == 'numeric':
            s = 'DELETE FROM %s WHERE ID=:1' % (self.q_name,)
            self.dbpool_export.runOperation(s, (mid,)).addCallback(lambda x:self.deleteMessageSuccess(x, mid)).addErrback(lambda x:self.deleteMessageFailure(x, mid))
        elif self.dbpool_export.dbapi.paramstyle == 'named':
            s = 'DELETE FROM %s WHERE ID=:mid' % (self.q_name,)
            self.dbpool_export.runOperation(s, {'mid':mid}).addCallback(lambda x:self.deleteMessageSuccess(x, mid)).addErrback(lambda x:self.deleteMessageFailure(x, mid))
        else:
            log.msg('DatabaseQueueService:unknown paramstyle %s for dbapi %s' % (self.dbpool_export.dbapi.paramstyle,self.dbpool_export.dbapiName))
    def _storeMessageFailure(self, result, mid):
        self.deleteMessage(mid)
        self.unprocessed = self.unprocessed - 1
        if self.paused:
            if self.unprocessed < self.max_unprocessed:
                self.continueFetching()
    def _storeMessageSuccess(self, failure, mid):
        self.stored = self.stored + 1
        #self.printStat()
        self.deleteMessage(mid)
        self.unprocessed = self.unprocessed - 1
        if self.paused:
            if self.unprocessed < self.max_unprocessed:
                self.continueFetching()
    def _storeMessage(self, message, mid):
        self.storeMessage(message).addCallback(lambda x: self._storeMessageSuccess(x,mid)).addErrback(lambda x: self._storeMessageFailure(x,mid))
    def storeMessage(self, message):
        d = defer.Deferred()
        d.callback(True)
        return d
    def fetchMessageSuccess(self, result, mid):
        if len(result) > 0:
            message = result[0][0]
            #mid = result[0][1]
            if None != message:
                self.fetched = self.fetched + 1
                #self.printStat()
                self._storeMessage(message, mid)
                self.unprocessed = self.unprocessed + 1
                if not self.paused:
                    if self.unprocessed > self.max_unprocessed:
                        self.pauseFetching(mid)
                    else:
                        self.fetchMessage(mid + 1)
            else:
                log.msg('DatabaseQueueService: message %d is empty' % (mid,))
                reactor.callLater(3, self.fetchMessage, mid + 1)
        else:
            reactor.callLater(3, self.fetchMessage, mid)
    def pauseFetching(self, mid):
        self.paused = True
        self.paused_mid = mid
        log.msg('DatabaseQueueService: pause on message %d, there are %d unprocessed message(s)' % (self.paused_mid, self.unprocessed,))
    def continueFetching(self):
        if None != self.paused_mid:
            log.msg('DatabaseQueueService: continue from message %d' % (self.paused_mid + 1,))
            self.fetchMessage(self.paused_mid + 1)
            self.paused = False
            self.paused_mid = None
        else:
            log.msg('DatabaseQueueService: there are no paused message id')
    def fetchMessageFailure(self, failure, mid):
        log.msg('DatabaseQueueService: fetch message %d error: %s' % (mid, failure))
        reactor.callLater(3, self.fetchMessage, mid)
    def fetchMessage(self, mid):
        #s = 'SELECT PACKET,ID FROM %s ORDER BY ID LIMIT 1' % (self.q_name,)
        #self.dbpool_export.runQuery(s).addCallback(lambda x: self.fetchMessageSuccess(x, mid)).addErrback(lambda x: self.fetchMessageFailure(x, mid))    
        if self.dbpool_export.dbapi.paramstyle == 'format':
            s = 'SELECT PACKET FROM %s WHERE ID=%%s' % (self.q_name,)
            self.dbpool_export.runQuery(s, (mid,)).addCallback(lambda x: self.fetchMessageSuccess(x, mid)).addErrback(lambda x: self.fetchMessageFailure(x, mid))
        elif self.dbpool_export.dbapi.paramstyle == 'qmark':
            s = 'SELECT PACKET FROM %s WHERE ID=?' % (self.q_name,)
            self.dbpool_export.runQuery(s, (mid,)).addCallback(lambda x: self.fetchMessageSuccess(x, mid)).addErrback(lambda x: self.fetchMessageFailure(x, mid))
        elif self.dbpool_export.dbapi.paramstyle == 'pyformat':
            s = 'SELECT PACKET FROM %s WHERE ID=%%(mid)s' % (self.q_name,)
            self.dbpool_export.runQuery(s, {'mid':mid}).addCallback(lambda x: self.fetchMessageSuccess(x, mid)).addErrback(lambda x: self.fetchMessageFailure(x, mid))            
        elif self.dbpool_export.dbapi.paramstyle == 'numeric':
            s = 'SELECT PACKET FROM %s WHERE ID=:1' % (self.q_name,)
            self.dbpool_export.runQuery(s, (mid,)).addCallback(lambda x: self.fetchMessageSuccess(x, mid)).addErrback(lambda x: self.fetchMessageFailure(x, mid))
        elif self.dbpool_export.dbapi.paramstyle == 'named':
            s = 'SELECT PACKET FROM %s WHERE ID=:mid' % (self.q_name,)
            self.dbpool_export.runQuery(s, {'mid':mid}).addCallback(lambda x: self.fetchMessageSuccess(x, mid)).addErrback(lambda x: self.fetchMessageFailure(x, mid))
        else:
            log.msg('DatabaseQueueService:unknown paramstyle %s for dbapi %s' % (self.dbpool_export.dbapi.paramstyle,self.dbpool_export.dbapiName))
    def getMinIdSuccess(self, result):
        mid = result[0][0]
        if None != mid:
            log.msg('DatabaseQueueService: starting from message %d' % (mid,))
            self.fetchMessage(mid)
        else:
            reactor.callLater(3, self.findMinId)
    def getMinIdFailure(self, failure):
        log.msg('DatabaseQueueService: error finding min queue id: %s' % (failure.getTraceback()))
        reactor.callLater(3, self.findMinId)
    def findMinId(self):
        self.dbpool_export.runQuery('SELECT MIN(ID) FROM %s' % (self.q_name,)).addCallback(self.getMinIdSuccess).addErrback(self.getMinIdFailure)
    def startService(self):
        reactor.callLater(3, self.findMinId)
        #reactor.callLater(3, self.fetchMessage, 0)
        Service.startService(self)
    def stopService(self):
        return Service.stopService(self)
