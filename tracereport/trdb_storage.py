import json
from datetime import datetime
import cStringIO

from twisted.python import log
from twisted.internet import defer, reactor
from ustar.dbqueues import DatabaseQueueService

class TrdbStorageService(DatabaseQueueService):
    def __init__(self, dbpool_export, dbpool_storage, q_name):
        DatabaseQueueService.__init__(self, dbpool_export, q_name)
        self.dbpool_storage = dbpool_storage
    def storeMessageFailure(self, failure, fields, d):
        log.msg('TrdbStorageService: error storing message %s: %s' % (fields, failure.getTraceback()))
        if None != failure.check(self.dbpool_storage.dbapi.OperationalError, self.dbpool_storage.dbapi.InternalError):
            reactor.callLater(3, self.doStoreMessage, fields, d)
        else:
            self.messageStored(d)
    def messageStored(self, d):
        d.callback(True)
    def storeMessageSuccess(self, result, fields, d):
        self.messageStored(d)
    def doStoreMessage(self, fields, d):
        io = cStringIO.StringIO()

        io.write('INSERT INTO TELEDATA (')
        f_c = 0
        for k in fields.keys():
            if f_c > 0:
                io.write(',')
            io.write(k)
            f_c = f_c + 1

        io.write(') VALUES (')
        f_c = 0
        for k in fields.keys():
            if f_c > 0:
                io.write(',')
            io.write(fields[k])
            f_c = f_c + 1

        io.write(')')
        sqlinsert = io.getvalue()
        io.close()

        self.dbpool_storage.runOperation(sqlinsert).addCallback(lambda x: self.storeMessageSuccess(x, fields, d)).addErrback(lambda x: self.storeMessageFailure(x, fields, d))
    def storeMessage(self, message):
        df = defer.Deferred()
        try:
            pack = json.loads(message)
            if pack['Type'] == 'Report':
                gps_fix = False
                gps_gprmc = False
                fields = {}
                fields['RS485_1'] = '0'
                fields['RS485_2'] = '0'
                fields['RS485_3'] = '0'
                fields['RS485_4'] = '0'
                fields['RS485_5'] = '0'
                fields['RS485_6'] = '0'
                fields['RS485_7'] = '0'
                fields['RS485_8'] = '0'
                fields['OBJID'] = '%d' % (pack['SenderId'])
                for p in pack['Payload']:
                    if p['Type'] == 'Index':
                        if None != p['FlashAddr']:
                            fields['FLASH_ADDR'] = '%d' % (p['FlashAddr'],)
                        if None != p['StateTime']:
                            fields['STATE_DATE_TIME'] = '"%s"' % (datetime.fromtimestamp(p['StateTime']),)
                    if p['Type'] == 'Location':
                        for l in p['Data']:
                            if l['Type'] == 'Value':
                                fields['LONGITUDE'] = '%f' % (l['Longitude'],)
                                fields['LATITUDE'] = '%f' % (l['Latitude'],)
                                if l['Altitude'] != None:
                                    fields['ALTITUDE'] = '%d' % (l['Altitude'],)
                                fields['COURSE'] = '%d' % (l['Course'],)
                                fields['SPEED'] = '%f' % (l['Speed'],)
                            if l['Type'] == 'Reliability':
                                if None !=l['FixTime']:
                                    fields['LOC_DATE_TIME'] = '"%s"' % (datetime.fromtimestamp(l['FixTime']),)
                                gps_fix = l['Fix']
                                if gps_fix:
                                    gps_gprmc = True
                                if None != l['Satellites']:
                                    fields['SATTELITS'] = '%d' % (l['Satellites'],)
                    if p['Type'] == 'EquipSupply':
                        if None != p['Power']:
                            #fields['VOLTAGE'] = '%d' % (int(1000 * p['Power']),)
                            fields['VOLTAGE'] = '%f' % (p['Power'] / 12.25)
                            if p['Power'] < 5.:
                                if None != p['Battery']:
                                    fields['VOLTMETER_1'] = '%f' % (p["Battery"] / 12.25,)
                            else:
                                fields['VOLTMETER_1'] = '%f' % (p["Power"] / 12.25,)
                        if None != p['Battery']:
                            #fields['BATTERY'] = '%d' % (int(1000 * p["Battery"]),)
                            fields['BATTERY'] = '%f' % (p["Battery"] / 2.)
                    if p['Type'] == 'Outs':
                        for out in p['Data']:
                            if out['Type'] == 'Key':
                                outs_bitmap = 0
                                l = len(out['Values'])
                                if l > 0 and None != out['Values'][0]:
                                    if out['Values'][0] == 'on':
                                        outs_bitmap = outs_bitmap | 0x01
                                if l > 1 and None != out['Values'][1]:
                                    if out['Values'][1] == 'on':
                                        outs_bitmap = outs_bitmap | 0x02
                                if l > 2 and None != out['Values'][2]:
                                    if out['Values'][2] == 'on':
                                        outs_bitmap = outs_bitmap | 0x04
                                if l > 3 and None != out['Values'][3]:
                                    if out['Values'][3] == 'on':
                                        outs_bitmap = outs_bitmap | 0x08
                                fields['OUTS'] = '%d' % (outs_bitmap,)
                    if p['Type'] == 'CanBus':
                        for d in p['Data']:
                            if d['Type'] == 'FMS':
                                if None != d['TotalFuel']:
                                    fields['FMS_TOTALFUEL'] = '%f' % (d['TotalFuel'])
                                if None != d['TotalDistance']:
                                    fields['FMS_TOTALDISTANCE'] = '%f' % (d['TotalDistance'])
                                if None != d['FuelLevel']:
                                    fields['FMS_FUELLEVEL'] = '%d' % (int(d['FuelLevel']))
                                if None != d['CoolantTemperature']:
                                    fields['FMS_COOLANTTEMP'] = '%d' % (int(d['CoolantTemperature']))
                                if None != d['RPM']:
                                    fields['FMS_RPM'] = '%d' % (int(d['RPM']))
                    if p['Type'] == 'Inputs':
                        for inp in p['Data']:
                            if inp['Type'] == 'Voltmeter':
                                outs_bitmap = 0
                                l = len(inp['Values'])
                                #if l > 0 and None != inp['Values'][0]:
                                    #fields['VOLTMETER_1'] = '%f' % (inp['Values'][0] / 12.25,)
                                if l > 1 and None != inp['Values'][1]:
                                    fields['VOLTMETER_2'] = '%f' % (inp['Values'][1] / 12.25,)
                                if l > 2 and None != inp['Values'][2]:
                                    fields['VOLTMETER_3'] = '%f' % (inp['Values'][2] / 12.25,)
                                if l > 3 and None != inp['Values'][3]:
                                    fields['VOLTMETER_4'] = '%f' % (inp['Values'][3] / 12.25,)
                            if inp['Type'] == 'FuelSensor':
                                for inp_v in inp['Values']:
                                    s_i = inp_v['SensorId'] % 10
                                    fields['RS485_%d' % (s_i,)] = '%d' % (inp_v['SensorOutput'],)
                                    if not fields.has_key('STATE_DATE_TIME'):
                                        if None != inp_v['MeasurementTime']:
                                            fields['STATE_DATE_TIME'] = '"%s"' % (datetime.fromtimestamp(inp_v['MeasurementTime']),)
                            if inp['Type'] == 'Temperature':
                                l = len(inp['Values'])
                                if l > 0 and None != inp['Values'][0]:
                                    fields['TEMPERATURE_1'] = '%d' % (int(inp['Values'][0]),)
                                if l > 1 and None != inp['Values'][1]:
                                    fields['TEMPERATURE_2'] = '%d' % (int(inp['Values'][1]),)
                                if l > 2 and None != inp['Values'][2]:
                                    fields['TEMPERATURE_3'] = '%d' % (int(inp['Values'][2]),)
                                if l > 3 and None != inp['Values'][3]:
                                    fields['TEMPERATURE_4'] = '%d' % (int(inp['Values'][3]),)


                    if p['Type'] == 'AlarmZones':
                        l = len(p['Now'])
                        now_bitmap = 0
                        if l > 0:
                            if p['Now'][0].lower() == 'signal':
                                now_bitmap = now_bitmap | 0x0001
                            if p['Now'][0].lower() == 'short':
                                now_bitmap = now_bitmap | 0x0002
                            if p['Now'][0].lower() == 'open':
                                now_bitmap = now_bitmap | 0x0003
                        if l > 1:
                            if p['Now'][1].lower() == 'signal':
                                now_bitmap = now_bitmap | 0x0004
                            if p['Now'][1].lower() == 'short':
                                now_bitmap = now_bitmap | 0x0008
                            if p['Now'][1].lower() == 'open':
                                now_bitmap = now_bitmap | 0x000c
                        if l > 2:
                            if p['Now'][2].lower() == 'signal':
                                now_bitmap = now_bitmap | 0x0010
                            if p['Now'][2].lower() == 'short':
                                now_bitmap = now_bitmap | 0x0020
                            if p['Now'][2].lower() == 'open':
                                now_bitmap = now_bitmap | 0x0030
                        if l > 3:
                            if p['Now'][3].lower() == 'signal':
                                now_bitmap = now_bitmap | 0x0040
                            if p['Now'][3].lower() == 'short':
                                now_bitmap = now_bitmap | 0x0080
                            if p['Now'][3].lower() == 'open':
                                now_bitmap = now_bitmap | 0x00c0
                        if l > 4:
                            if p['Now'][4].lower() == 'signal':
                                now_bitmap = now_bitmap | 0x0100
                            if p['Now'][4].lower() == 'short':
                                now_bitmap = now_bitmap | 0x0200
                            if p['Now'][4].lower() == 'open':
                                now_bitmap = now_bitmap | 0x0300
                        if l > 5:
                            if p['Now'][5].lower() == 'signal':
                                now_bitmap = now_bitmap | 0x0400
                            if p['Now'][5].lower() == 'short':
                                now_bitmap = now_bitmap | 0x0800
                            if p['Now'][5].lower() == 'open':
                                now_bitmap = now_bitmap | 0x0c00
                        if l > 6:
                            if p['Now'][6].lower() == 'signal':
                                now_bitmap = now_bitmap | 0x1000
                            if p['Now'][6].lower() == 'short':
                                now_bitmap = now_bitmap | 0x2000
                            if p['Now'][6].lower() == 'open':
                                now_bitmap = now_bitmap | 0x3000
                        if l > 7:
                            if p['Now'][7].lower() == 'signal':
                                now_bitmap = now_bitmap | 0x4000
                            if p['Now'][7].lower() == 'short':
                                now_bitmap = now_bitmap | 0x8000
                            if p['Now'][7].lower() == 'open':
                                now_bitmap = now_bitmap | 0xc000
                        fields['LOOPS'] = '%d' % (now_bitmap,)
                        l = len(p['Alarms'])
                        alarms_bitmap = 0
                        if l > 0:
                            if p['Now'][0].lower() == 'signal':
                                alarms_bitmap = alarms_bitmap | 0x0001
                            if p['Now'][0].lower() == 'short':
                                alarms_bitmap = alarms_bitmap | 0x0002
                            if p['Now'][0].lower() == 'open':
                                alarms_bitmap = alarms_bitmap | 0x0003
                        if l > 1:
                            if p['Now'][1].lower() == 'signal':
                                alarms_bitmap = alarms_bitmap | 0x0004
                            if p['Now'][1].lower() == 'short':
                                alarms_bitmap = alarms_bitmap | 0x0008
                            if p['Now'][1].lower() == 'open':
                                alarms_bitmap = alarms_bitmap | 0x000c
                        if l > 2:
                            if p['Now'][2].lower() == 'signal':
                                alarms_bitmap = alarms_bitmap | 0x0010
                            if p['Now'][2].lower() == 'short':
                                alarms_bitmap = alarms_bitmap | 0x0020
                            if p['Now'][2].lower() == 'open':
                                alarms_bitmap = alarms_bitmap | 0x0030
                        if l > 3:
                            if p['Now'][3].lower() == 'signal':
                                alarms_bitmap = alarms_bitmap | 0x0040
                            if p['Now'][3].lower() == 'short':
                                alarms_bitmap = alarms_bitmap | 0x0080
                            if p['Now'][3].lower() == 'open':
                                alarms_bitmap = alarms_bitmap | 0x00c0
                        if l > 4:
                            if p['Now'][4].lower() == 'signal':
                                alarms_bitmap = alarms_bitmap | 0x0100
                            if p['Now'][4].lower() == 'short':
                                alarms_bitmap = alarms_bitmap | 0x0200
                            if p['Now'][4].lower() == 'open':
                                alarms_bitmap = alarms_bitmap | 0x0300
                        if l > 5:
                            if p['Now'][5].lower() == 'signal':
                                alarms_bitmap = alarms_bitmap | 0x0400
                            if p['Now'][5].lower() == 'short':
                                alarms_bitmap = alarms_bitmap | 0x0800
                            if p['Now'][5].lower() == 'open':
                                alarms_bitmap = alarms_bitmap | 0x0c00
                        if l > 6:
                            if p['Now'][6].lower() == 'signal':
                                alarms_bitmap = alarms_bitmap | 0x1000
                            if p['Now'][6].lower() == 'short':
                                alarms_bitmap = alarms_bitmap | 0x2000
                            if p['Now'][6].lower() == 'open':
                                alarms_bitmap = alarms_bitmap | 0x3000
                        if l > 7:
                            if p['Now'][7].lower() == 'signal':
                                alarms_bitmap = alarms_bitmap | 0x4000
                            if p['Now'][7].lower() == 'short':
                                alarms_bitmap = alarms_bitmap | 0x8000
                            if p['Now'][7].lower() == 'open':
                                alarms_bitmap = alarms_bitmap | 0xc000
                        fields['LOOPS_CHANGED'] = '%d' % (alarms_bitmap,)
                state_bitmap = 0
                if gps_fix:
                    state_bitmap = state_bitmap | 0x02
                if gps_gprmc:
                    state_bitmap = state_bitmap | 0x01
                fields['FLAGS'] = '%d' % (state_bitmap,)
                if fields.has_key('STATE_DATE_TIME') or fields.has_key('LOC_DATE_TIME'):
                    self.doStoreMessage(fields, df)
                else:
                    log.msg('TrdbStorageService: there are no timestamps in "%s"' % (pack,))
                    df.errback(Exception('there are no timestamps'))
            else:
                log.msg('TrdbStorageService: there are no reports in "%s"' % (pack,))
                df.errback(Exception('there are no reports'))
        except Exception, why:
                import traceback
                traceback.print_exc()
                df.errback(why)
        return df
