from twisted.application.service import Application, IServiceCollection
from twisted.enterprise import adbapi

import json

from tracewatch.clients import FrontendClientService
from ustar.dbqueues import KnockerOneDBqueueService

gw_ip = 'assistant.internal'
rpc_port = 6081

application = Application("Knocker dbqueue service")
service_coolection = IServiceCollection(application)


class ClientService(FrontendClientService):
	def __init__(self, ip, port, knocker_service):
		FrontendClientService.__init__(self, ip, port)
		self.knocker_service = knocker_service
		
	def stateReport(self, serial, ts, data):
		s = json.loads(data)
		self.knocker_service.putMessage(s)
		    
dbhost_export = 'localhost'
dbport_export = 3306
dbname_export = 'export'
dbuser_export = 'q_writer'
dbpassword_export = 'p@ssw0rd'

import MySQLdb
dbpool_export = adbapi.ConnectionPool("MySQLdb", host = dbhost_export, port = dbport_export, db = dbname_export, user = dbuser_export, passwd = dbpassword_export, cp_reconnect = True, cp_noisy = True)

knocker_service = KnockerOneDBqueueService(dbpool_export, 'BUFFER')
service_coolection.addService(knocker_service)

client_service = ClientService(gw_ip, rpc_port, knocker_service)
service_coolection.addService(client_service)


