from twisted.internet import reactor
from twisted.enterprise import adbapi

from ustar.dbqueues import KnockerOneDBqueueService

gw_ip = 'assistant.internal'
rpc_port = 6081

dbhost_export = 'localhost'
dbport_export = 3306
dbname_export = 'export'
dbuser_export = 'q_writer'
dbpassword_export = 'p@ssw0rd'

import MySQLdb
dbpool_export = adbapi.ConnectionPool("MySQLdb", host = dbhost_export, port = dbport_export, db = dbname_export, user = dbuser_export, passwd = dbpassword_export, cp_reconnect = True, cp_noisy = True)

knocker_service = KnockerOneDBqueueService(dbpool_export, 'BUFFER')

def deleteTableSuccess(result):
    print 'Table deleted'
    reactor.stop()

def deleteTableFailure(failure):
    print 'Deleting table error:%s' % (failure.getTraceback(),)
    reactor.stop()

knocker_service.deleteTable(deleteTableSuccess, deleteTableFailure)
reactor.run()


